package main

import (
"fmt"
"net/http"
)
const WHOM = "Serdar"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", WHOM)
	})
	//
	//http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
	//	fmt.Fprintf(w, "Healthy!")
	//})

	http.ListenAndServe(":8999", nil)
}