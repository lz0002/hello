FROM gitlab-registry.eps.surrey.ac.uk/lz0002/golang:latest

# Set the Current Working Directory inside the container
WORKDIR /go/src/hello

# Add all project files into the container
ADD . .

# Build the Go app
RUN go build -o hello .


# This container exposes port 8999
EXPOSE 8999

# Run the binary program produced by `go build`
CMD ["./hello"]
